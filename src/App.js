import React, {Component} from 'react';
import {connect} from 'react-redux';
import CommentsBox from './components/CommentsBox';

/**
 * Main Wrapper
 */
class App extends Component {
   
    render() {
        return (
            <div className="content-container">
                <CommentsBox />
            </div>
        )
    }
}

export default connect()(App);