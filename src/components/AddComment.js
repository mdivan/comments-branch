import React from 'react';
import {connect} from 'react-redux';
import {startAddComment} from '../actions/comments';
import CommentForm from './CommentForm';

export class AddComment extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddComment = this.handleAddComment.bind(this);
    }

    handleAddComment(comment) {
       return this.props.startAddComment(comment);
    };

    render() {
        return (
            <CommentForm 
                handleAddComment={this.handleAddComment}/>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startAddComment: (comment) => dispatch(startAddComment(comment))
});

export default connect(undefined, mapDispatchToProps)(AddComment);