import React from 'react';

class CommentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            error: '',
            isLoading: false
        }
        this.handleCommentChange = this.handleCommentChange.bind(this);
        this.handleSubmitComment = this.handleSubmitComment.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleCommentChange(e) {
        const comment = e.target.value;
        this.setState(() => ({ error: '', comment }));
    }

    handleSubmitComment() {
        this.setState(() => ({isLoading: true}))
        this.props.handleAddComment(this.state.comment).then((error) => {
            this.setState(() => ({comment: '', isLoading: false, error}));
        });
    }

    handleKeyPress(e) {
        if(e.key === 'Enter') {
            e.preventDefault();
          this.handleSubmitComment()  
        }
    }

    render() {
        return (
            <form 
                className='form'>
            <div className='img-box img-box--bottom'>

            </div>
            <div>
                <p className='form__error'>{this.state.error}</p>
                <textarea 
                    disabled={this.state.isLoading}
                    className='textarea'
                    onChange={this.handleCommentChange}
                    placeholder='write comment..' 
                    onKeyPress={this.handleKeyPress}
                    value={this.state.comment} />
            </div>
            </form>
        )
    }
}

export default CommentForm;