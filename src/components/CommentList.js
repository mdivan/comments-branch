import React from 'react';
import { connect } from 'react-redux';

const CommentList = (props) => (
    <div>
        <hr/>
        <div className='list'>
        {props.comments.map((comment, index) => {
            return (<div 
                     className='list__item'
                     key={index}>
                     <div className='img-box'>
                     </div>
                        <p className='comment'>{comment}</p>
                     </div>)
        })}
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    comments: state.comments
});

export default connect(mapStateToProps)(CommentList);