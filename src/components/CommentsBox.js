import React from 'react'
import CommentList from './CommentList'
import AddComment from './AddComment'

const CommentsBox = () => (
    <div>
        <AddComment />
        <CommentList />
    </div>
);

export default CommentsBox;