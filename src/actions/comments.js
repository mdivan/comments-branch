import db from '../database/db';

export const addComment = (comment) => ({
    type: 'ADD_COMMENT',
    comment
});

export const startAddComment = (comment) => {
    return (dispatch) => {
        return db(comment).then((res) => {
            if(res.success) {
                dispatch(addComment(comment));
                return '';
            }
            else {
                return res.error;
            }
        });
    }
}