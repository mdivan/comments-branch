export default (state = [], action) => {
    switch(action.type) {
        case 'ADD_COMMENT':
            return [action.comment, ...state];
        default:
            return state;
    }
}