export default (comment) => {
    let success = false;
    return new Promise((resolve) => {
        return setTimeout(() => {
            success = addComment(comment);
            const response  = {
                error,
                success
            }
            resolve(response);
        }, 1500);
    });
}

let error = '';

const addComment = (comment) => {
    return validateComment(comment);
}

const validateComment = (comment) => {
    if(!comment) {
        error = "can't add empty comment"
        return false;
    }
    if(!validateMaxLength(comment, 25)) {
        error = `Max characters allowed is ${25}`;
        return false;
    }
    return true;
}

const validateMaxLength = (txt, length) => {
    return txt.length < length;
}
