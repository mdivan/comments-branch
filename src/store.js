import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'
import thunk from 'redux-thunk'
import commentsReducer from './reducers/comments'

let initialState = {};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const Store = createStore(
    combineReducers({
        routing: routerReducer,
        comments: commentsReducer
    }),
    initialState,
    composeEnhancers(applyMiddleware(thunk)),
    compose(
        applyMiddleware(routerMiddleware(hashHistory))
    )
);


export default Store;

